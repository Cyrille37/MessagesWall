<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['prefix' => 'Sms/Nexmo', 'namespace' => 'Sms'], function ()
{
	//Route::get('/', 'SmsController@nexmo');
	Route::post('/', 'SmsController@nexmo');
});

Route::group(['prefix' => 'sms/sms2web', 'namespace' => 'Sms'], function ()
{
	Route::post('/', 'SmsController@sms2web_post');
});

Route::group(['prefix' => 'MsgWall', 'namespace' => 'MsgWall'], function ()
{
	Route::get('/messages/statusChanged/{since?}', 'DisplayController@messagesStatusChanged');
	Route::get('/messages/from/{fromId?}', 'ManagerController@messages');
	Route::put('/messages/{msgId}/statusChange/{newStatus}', 'ManagerController@statusChange');

	Route::get('/options/{optionKey}', 'ManagerController@getOption');
	Route::put('/options/{optionKey}/{optionValue}', 'ManagerController@setOption');

});
