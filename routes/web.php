<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
	
Route::group(['prefix' => 'Manager', 'namespace' => 'MsgWall'], function ()
{
	Route::get('/', 'ManagerController@index');
});

Route::group(['prefix' => 'Display', 'namespace' => 'MsgWall'], function ()
{
	Route::get('/', 'DisplayController@index');
});

Route::group(['prefix' => 'Twitter', 'namespace' => 'Twitter'], function ()
{
	Route::get('/', 'TwitterController@index');
	Route::get('/search', 'TwitterController@search');
	Route::post('/creds', 'TwitterController@setCreds');
	Route::get('/creds', 'TwitterController@creds');
	Route::get('/resetSearch', 'TwitterController@resetSearch');
});

Route::group(['prefix' => 'Sms', 'namespace' => 'Sms'], function ()
{
	Route::get('/', 'SmsController@index');
});
