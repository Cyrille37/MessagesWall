/**
 * Vue application: Manager
 */

var manager = new Vue(
{
	el: '#manager-app',
	data:
	{
		/**
		 * Array of managed messages
		 */
		messages: [],
		messagesCount: {
			'new': 0 , onair: 0, trash: 0
		},
		lastLoadedId: 0,
		lastLoadedMessagesCount: 0,
		getMessagesTimer: '',
		getMessagesFreq: 5000,
		error: ''
	},
	computed: {
	},
	created: function () {
	},
	mounted: function()
	{
		this.getMessages();
	},
	beforeDestroy: function()
	{
		this.cancelAutoUpdate();
	},
	methods:
	{
	    cancelAutoUpdate: function()
	    {
		    clearInterval( this.getMessagesTimer );
		},

		onMessagesCountChange: function( status, count)
		{
			switch( status )
			{
			case 'new': this.messagesCount.new = count ; break;
			case 'onair': this.messagesCount.onair = count ; break;
			case 'trash': this.messagesCount.trash = count ; break;
			}
		},

		/**
		 * Change a message's status
		 */
		onChangeStatus: function( event )
		{
			var msg = this.messages.find( function(msg){
				return msg.id == event.id ;
			});
			if( typeof msg === 'undefined'){
				Error('msg id "'+event.id+'" not found (manager.onChangeStatus)');
				return ;
			}
			msg.status = event.status ;
			
			var that = this ;
			//$.getJSON('/api/MsgWall/messages/'+msg.id+'/statusChange/'+event.status)
			$.ajax(
			{
				url: '/api/MsgWall/messages/'+msg.id+'/statusChange/'+event.status,
				dataType: "json",
				method: 'PUT'
			})
			.done(function(json)
			{
				console.log( json );
			})
			.fail(function(jqxhr, textStatus, error)
			{
				try
				{
					var json = $.parseJSON( jqxhr.responseText );
					that.error = json.error.message ;
				}
				catch(ex)
				{
					var matches = jqxhr.responseText.match( /<span class="exception_message">(.*)<\/span>/ );
					that.error = matches[1] ;
				}
			});

		},

		/*
		 * Add an array of messages, or only one message.
		 * 
		 * messagesAdd: function( newMessages )
		{
			if( Array.isArray(newMessages) )
			{
				// note: Why binding refresh 2 times ???
				var self = this ;
				newMessages.forEach(function(msg)
				{
					msg.uid = (++ self.lastUid );
					//self.messages.push( msg );
				});
				// Do not refresh binding:
				//Array.prototype.push.apply( self.messages, newMessages);
				// (ES6) Refresh binding: 
				this.messages.push( ...newMessages );				
			}
			else
			{
				newMessages.uid = (++ this.lastUid) ;
				this.messages.push( newMessages );
			}
			
		},*/

		getMessages: function( optionalThat )
		{
			var that = this ;

			var button = null ;
			if( typeof optionalThat !== 'undefined' )
			{
				if( optionalThat instanceof MouseEvent )
				{
					button = event.target ;
					button.disabled = true ;					
				}
				else
				{
					that = optionalThat ;
				}
			}

			$.getJSON('/api/MsgWall/messages/from/'+this.lastLoadedId)
			.done(function( messages )
			{
				console.log( 'messages loaded: '+messages.length );
				if( messages instanceof Array)
				{
					messages.forEach( function(msg)
					{
						if( msg.id > that.lastLoadedId )
							that.lastLoadedId = msg.id ;
					});				
					that.messages.push( ...messages );
					that.lastLoadedMessagesCount = messages.length ;
				}
				else
					that.lastLoadedMessagesCount = 0 ;
			})
			.fail(function(jqxhr, textStatus, error)
			{
				try
				{
					var json = $.parseJSON( jqxhr.responseText );
					that.error = json.error.message ;
				}
				catch(ex)
				{
					var matches = jqxhr.responseText.match( /<span class="exception_message">(.*)<\/span>/ );
					that.error = matches[1] ;
				}
			})
			.always(function()
			{
				if( button != null )
					button.disabled = false ;
				
				that.getMessagesTimer = setTimeout( that.getMessages, that.getMessagesFreq, that );
			});


		}

	}

});
