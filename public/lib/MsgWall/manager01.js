/**
 * 
 */

var app ;

Vue.component('tweets-component', {
	props: [],
	template:
		'<div class="tweetsList"><hr/><ul>'
		+'<tweet-component v-for="(tweet, index) in tweets" v-bind:tweet="tweet" v-bind:index="index" v-on:removetweet="removeTweet"></tweet>'
		+'</ul>'
		+'<button v-on:click="add">add</button>'
		+'<hr/></div>',
	data: function(){
		return {
			tweets:[
				{ text: '1er tweet' },
				{ text: 'second tweet' },
				{ text: 'tweet troisième' }
			]
		};
	},
	methods: {
		test: function(){console.log('tweets-component.test()'); console.log(this)},
		removeTweet: function( index )
		{
			console.log('tweets-component.removeTweet()');
			console.log( index+'/'+this.tweets.length );
			this.tweets.splice( index, 1 );
			this.$emit('tweet-removed');
		},
		add: function()
		{
			this.tweets.push( { text: 'nouveau tweet '+(this.tweets.length+1) } );
			console.log( $(this.$el) );
			var that = $(this.$el);
		    this.$nextTick( () => {
		        //const listItem = document.querySelector( $(this.$el) );
		    	//const listItem = $(this.$el) ;
		    	const listItem = that ;
		        listItem.scrollTop = listItem.scrollHeight;
		    });
		}

	},
	components: {
		'tweet-component': {
			props: ['tweet', 'index'],
			template:
				'<li>'
				+'<div>{{ tweet.text }}</div>'
				+'<div><button v-on:click="remove(index)">remove</button></div>'
				+'</li>',
			methods: {
				remove: function(){
					console.log('tweet-component.remove() '+ this.index);
					this.$emit('removetweet', this.index);
				}
			}
		}		
	}
})

$(function()
{
	app = new Vue({
		el: '#app',
		data: {
			message: 'Hello Vue!',
		},
		methods: {
			onTweetRemoved: function()
			{
				console.log('app.onTweetRemoved()');				
			},
			tweetAdd: function()
			{
				var l = this.$refs.tweetsListOne ;
				l.tweets.push( { text: 'nouveau tweet '+(l.tweets.length+1) } );
			}
		}

	})

});
