/**
 * Vue application: Display
 */

var display = new Vue(
{
	el: '#display-app',
	data:
	{
		messages: [],
		messagesCount: {
			lastLoaded: 0, current: 0, next: 0
		},
		// after first load (since==0),
		// new messages are stored then displayed one by one at displayFreq time.
		nextMessages: [],
		// timestamp for last status_changed, to get fresh status changed messages
		since: 0,
		displayFreq: 5000,
		displayTimer: '',
		loadMsgFreq: 4 * 1000,
		loadMsgTimer: '',
		error: ''
	},
	created: function()
	{
	},
	mounted: function()
	{
		this.getMessages();
		this.displayNextMessage();
	},
	beforeDestroy: function()
	{
		this.cancelAutoUpdate();
	},
	methods:
	{
	    cancelAutoUpdate: function()
	    {
		    clearInterval( this.loadMsgTimer );
		    clearInterval( this.displayTimer );
		},

		getMessages: function( optionalThat )
		{
			var that = this ;
			if( typeof optionalThat != 'undefined' && ! (optionalThat instanceof MouseEvent))
			{
				that = optionalThat ;
			}

			//console.log('this.since to '+that.since);

			$.getJSON('/api/MsgWall/messages/statusChanged/'+this.since)
			.done(function( newMsgs )
			{
				//console.log(newMsgs);
				if( ! Array.isArray(newMsgs) )
				{
					that.messagesCount.lastLoaded = 0 ;
					that.loadMsgTimer = setTimeout( that.getMessages, that.loadMsgFreq, that );
					return ;
				}
				that.messagesCount.lastLoaded = newMsgs.length ;

				var oldSince = that.since ;
				newMsgs.forEach( function(newMsg)
				{
					//console.log(that.messages.length);
					var existsMsgIdx = that.messages.findIndex( function (item)
					{
						return item.id == newMsg.id;
					});

					if( existsMsgIdx < 0 )
					{
						// add new message if "onair"
						if( newMsg.status == MSG_STATUS_ONAIR )
						{
							//console.log('add '+newMsg.id)
							if( oldSince > 0 )
							{
								// if not already in nextMessages
								if( that.nextMessages.findIndex( function (item)
								{
									return item.id == newMsg.id;
								}) < 0 )
								{
									that.nextMessages.push( newMsg );									
								}
							}
							else
							{
								that.messages.push( newMsg );
								that.updateEmoji();
							}
						}
					}
					else
					{
						// remove message if not "onair"
						//console.log('remove '+newMsg.id+' at '+existsMsgIdx)
						if( newMsg.status != MSG_STATUS_ONAIR )
						{
							that.messages.splice( existsMsgIdx, 1 );
						}
					}

					if( newMsg.status_changed_at > that.since )
					{
						that.since = newMsg.status_changed_at ;
						//console.log('change this.since to '+that.since);
					}

				});//newMsgs.forEach 

				that.messagesCount.current = that.messages.length ;
				that.messagesCount.next = that.nextMessages.length ;
			})
			.fail(function(jqxhr, textStatus, error)
			{
				try
				{
					var json = $.parseJSON( jqxhr.responseText );
					that.error = json.error.message ;
				}
				catch(ex)
				{
					var matches = jqxhr.responseText.match( /<span class="exception_message">(.*)<\/span>/ );
					that.error = matches[1] ;
				}
			})
			.always(function()
			{
				that.loadMsgTimer = setTimeout( that.getMessages, that.loadMsgFreq, that );
			});

		},

		updateEmoji: function()
		{
			Vue.nextTick(function ()
			{
				//console.log('Vue.nextTick');
				// DOM updated
				$('li.message.tweet > *').Emoji({
					//path:  '/vendor/jqueryemoji/img/apple40/'
					//path: 'https://raw.githubusercontent.com/rodrigopolo/emoji-assets/master/EmojiOne/40/'
					path: 'https://raw.githubusercontent.com/rodrigopolo/emoji-assets/master/Twemoji/40/'
				});
			})			
		},

		displayNextMessage: function(optionalThat)
		{
			var that = this ;
			if( typeof optionalThat != 'undefined' && ! (optionalThat instanceof MouseEvent))
			{
				that = optionalThat ;
			}

			if( that.nextMessages.length == 0 )
			{
				that.displayTimer = setTimeout( that.displayNextMessage, that.displayFreq, that );
				return ;
			}

			var msg = that.nextMessages.shift();
			that.messages.push( msg );

			that.updateEmoji();

			that.messagesCount.current = that.messages.length ;
			that.messagesCount.next = that.nextMessages.length ;

			that.displayTimer = setTimeout( that.displayNextMessage, that.displayFreq, that );
		}

	}
});
