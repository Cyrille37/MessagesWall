@extends('Twitter/_layout')

@section('title', 'Twitter')

@section('content')

	<h1>Twitter</h1>

	<a href="/Twitter/creds" >Set Twitter Credentials</a>,
	<a href="/Twitter/search" >Search Twitter</a>

<?php
	\App\Helpers\DebugHelper::dump( $twUser);
?>

@endsection
