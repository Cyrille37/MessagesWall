@extends('Twitter/_layout')

@section('title', 'Twitter')

@section('content')

	<h1>Twitter credentials</h1>

<form class="form-horizontal" action="/Twitter/creds" method="post">
	{{ csrf_field() }}
  <div class="form-group">
    <label for="ConsumerKey" class="col-sm-2 control-label">ConsumerKey</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="ConsumerKey" name="ConsumerKey" placeholder="ConsumerKey" />
    </div>
  </div>
  <div class="form-group">
    <label for="ConsumerSecret" class="col-sm-2 control-label">ConsumerSecret</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="ConsumerSecret" name="ConsumerSecret" placeholder="ConsumerSecret" />
    </div>
  </div>
  <div class="form-group">
    <label for="UserId" class="col-sm-2 control-label">UserId</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="UserId" name="UserId" placeholder="UserId" />
    </div>
  </div>
  <div class="form-group">
    <label for="AccessToken" class="col-sm-2 control-label">AccessToken</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="AccessToken" name="AccessToken" placeholder="AccessToken" />
    </div>
  </div>
  <div class="form-group">
    <label for="AccessTokenSecret" class="col-sm-2 control-label">AccessTokenSecret</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="AccessTokenSecret" name="AccessTokenSecret" placeholder="AccessTokenSecret" />
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Sign in</button>
    </div>
  </div>
</form>

			''
@endsection
