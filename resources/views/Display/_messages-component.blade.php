@verbatim

<script type="x-template" id="messages-component-template">

<ul class="list-group messagesList" v-sticky-scroll:animate="1500">
	<msg-component v-for="(msg, index) in messages"
		v-bind:msg="msg"
		v-bind:index="index"
	>
	</msg-component>
</ul>

</script>

<script type="x-template" id="messages-component-msg-template">

<li :class="'list-group-item img-rounded message' + (msg.type==MSG_TYPE_TWEET ? ' tweet' : msg.type==MSG_TYPE_SMS ? ' sms' : '')">
	
	<template v-if="msg.raw.entities && msg.raw.entities.media">
		<div class="media">
				<span v-for="(m, i) in msg.raw.entities.media">
					<div v-if="m.type=='photo'">
						<img class="twimg" :src="m.media_url+':small'" />
					</div>
					<span v-else > unknow media type <strong>{{m.type}}</strong> </span>
				</span>
		</div>
	</template>
	<template v-if="msg.type == MSG_TYPE_SMS">
		<h3 class="from">{{ msg.from.substring(0, 8).replace('+33', '0') + '...' }}</h3>
	</template>
	<template v-else>
		<h3 class="from">{{ msg.from }}</h3>
	</template>
	<p>{{ msg.text }}</p>
	
</li>

</script>

<script type="text/javascript">

Vue.component('messages-component',
{
	props: ['messages'],
	template: '#messages-component-template',
	data: function ()
	{
		return {
			messagesCount: 0
		}; 
	},
	methods:
	{
	},

	components: {

		'msg-component': {
			props: ['msg', 'index'],
			template: '#messages-component-msg-template',
			methods:
			{
			},
			mounted: function(event)
			{
				switch( this.msg.type )
				{
				case MSG_TYPE_TWEET:
					// First time we see a message of type 'tweet', parse it's 'raw' property has json.
					if( ! this.msg.rawHasJson )
					{
						this.msg.raw = $.parseJSON(this.msg.raw) ;
						this.msg.rawHasJson = true ;	
					}
					break;

				case MSG_TYPE_SMS:
					break;
				}

			}

		}	

	}

})

</script>

@endverbatim
