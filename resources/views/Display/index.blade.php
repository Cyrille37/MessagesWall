
@extends('Display/_layout')

@section('title', 'Messages Display')

@push('styles')
	<link href="/lib/MsgWall/display.css" rel="stylesheet" type="text/css" />
@endpush

@push('scripts-head')

	<script src="/vendor/jqueryemoji/js/jQueryEmoji.min.js"></script>
	<script>
		var MSG_TYPE_TWEET = '{{ \MessagesWall\Models\Message::TYPE_TWEET }}' ;
		var MSG_TYPE_SMS = '{{ \MessagesWall\Models\Message::TYPE_SMS }}' ;

		var MSG_STATUS_NEW = '{{ \MessagesWall\Models\Message::STATUS_NEW }}' ;
		var MSG_STATUS_ONAIR = '{{ \MessagesWall\Models\Message::STATUS_ONAIR }}' ;
		var MSG_STATUS_TRASH = '{{ \MessagesWall\Models\Message::STATUS_TRASH }}' ;
	</script>
@endpush

@push('scripts-body')
	<script src="/vendor/stickyScroll.js"></script>
	@include('Display._messages-component')
	<script src="/lib/MsgWall/display.js"></script>
@endpush

@section('content')

	<div id="display-app">

	<div>&nbsp;</div>

	@verbatim
	<div>
	<!--
		<button v-on:click="getMessages">pull</button>
		<button v-on:click="displayNextMessage">next</button>
		<span class="badge">{{ messagesCount.current }}</span>
		<span class="badge">{{ messagesCount.next }}</span>
		<span class="badge">{{ messagesCount.lastLoaded }}</span>
		<div v-if="error" class="alert alert-warning" role="alert">
			<button type="button" class="close" v-on:click="error = ''" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Warning!</strong> {{ error }}
		</div>
	-->
	</div>

	<div class="row">
		<div class="col-md-3 infoBox">

			<div style="font-size: 3.2em; text-align: center;" >

				<p style="margin-bottom: 18px; line-height: 95%;"><br/>Exprimez-vous<br/>en direct</p>
				<div style="font-size: 1.0em;">
					
					<strong>SMS</strong><br/> X XXX XXX XXX
					<br/>
					<strong>Twitter</strong><br/> #XxXx
				</div>

			</div>

		</div>
		<div class="col-md-9">
			<div class="">
				<messages-component 
					v-bind:messages="messages"
					></messages-component>
			</div>
		</div>
	</div>

		<div style="position: absolute; bottom: 20px;">
			<span class="badge">{{ messagesCount.current }}</span>
			<span class="badge">{{ messagesCount.next }}</span>
			<span class="badge">{{ messagesCount.lastLoaded }}</span>		
		</div>

	<div>
	<!--
		<span class="badge">{{ messagesCount.current }}</span>
		<span class="badge">{{ messagesCount.next }}</span>
		<span class="badge">{{ messagesCount.lastLoaded }}</span>
		<button v-on:click="getMessages">pull</button>
		<button v-on:click="displayNextMessage">next</button>

		<div v-if="error" class="alert alert-warning" role="alert">
			<button type="button" class="close" v-on:click="error = ''" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Warning!</strong> {{ error }}
		</div>
	-->
	</div>
	@endverbatim

	</div>

<script>
$(function()
{
});

function Error(message)
{
	//alert(message);
}
</script>

@endsection
