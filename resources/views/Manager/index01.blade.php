
@extends('Manager/_layout')

@section('title', 'Messages Manager')

@push('styles')
	<link href="/lib/Manager/manager.css" rel="stylesheet" type="text/css">
	<style type="text/css">
	.tweetsList{
		height: 200px;
		overflow: auto;
	}
	</style>
@endpush

@push('scripts')
	<script src="/js/app.js"></script>
	<script src="/lib/Manager/manager.js"></script>
@endpush

@section('content')

	<h1>Manager</h1>

<script>
//var message = "toto" ;
</script>

	<div id="app">

	  @{{ message }}
	  
	  <tweets-component ref="tweetsListOne" v-on:tweet-removed="onTweetRemoved"></tweets-component>

		<button v-on:click="tweetAdd">add</button>
	</div>
 
 
@endsection
