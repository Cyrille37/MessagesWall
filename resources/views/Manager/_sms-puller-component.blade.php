@verbatim

<script type="x-template" id="sms-puller-component-template">
<div class="panel panel-default" >
	<div class="panel-heading">Sms control</div>
	<div class="panel-body">
		<div v-if="error" class="alert alert-warning " role="alert">
			<button type="button" class="close" v-on:click="error = ''" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
			<strong>Warning!</strong> {{ error }}
		</div>
	</div>
</div>
</script>

<script type="text/javascript">

Vue.component('sms-puller-component',
{
	props: [],
	template: '#sms-puller-component-template',
	data: function ()
	{
		return {
			error: ''
		}; 
	}
});
</script>

@endverbatim
