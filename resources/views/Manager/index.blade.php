
@extends('Manager/_layout')

@section('title', 'Messages Manager')

@push('styles')
	<link href="/lib/MsgWall/manager.css" rel="stylesheet" type="text/css" />
@endpush

@push('scripts-head')
	<script>
	var MSG_TYPE_TWEET = '{{ \MessagesWall\Models\Message::TYPE_TWEET }}' ;
	var MSG_TYPE_SMS = '{{ \MessagesWall\Models\Message::TYPE_SMS }}' ;
	</script>
@endpush

@push('scripts-body')
	@include('Manager._display-settings-component')
	@include('Manager._tweets-puller-component')
	@include('Manager._sms-puller-component')
	@include('Manager._messages-component')
	<script src="/lib/MsgWall/manager.js"></script>
@endpush
@section('content')

	<div id="manager-app">

		<div class="row">

			@verbatim
			<div class="col-md-3" >
				<div class="panel panel-default" >
					<div class="panel-heading">
						Messages
						<span class="badge">{{ messages.length }}</span>
						<span class="badge">{{ lastLoadedMessagesCount }}</span>
					</div>
				  	<div class="panel-body">
				  		<div v-if="error" class="alert alert-warning" role="alert">
							<button type="button" class="close" v-on:click="error = ''" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Warning!</strong> {{ error }}
						</div>
						<!--
						<button v-on:click="getMessages">pull</button>
						-->
					</div>
				</div>
			</div>
			@endverbatim

			<div class="col-md-3">
				<display-settings-component					
				></display-settings-component>
			</div>

			<div class="col-md-3">
				<tweets-puller-component					
				></tweets-puller-component>
			</div>

			<div class="col-md-3">
				<sms-puller-component
				></sms-puller-component>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<a href="#">New <span class="badge">@{{ messagesCount.new }} </span></a>
				<messages-component class="messagesList"
					v-bind:messages="messages"
					status="new"
					v-on:messages-count-change="onMessagesCountChange"
				 	v-on:message-status-change="onChangeStatus($event)"
					></messages-component>
			</div>
			<div class="col-md-4">
				<a href="#">On air <span class="badge">@{{ messagesCount.onair }}</span></a>
				<messages-component class="messagesList"
					v-bind:messages="messages"
					status="onair"
					v-on:messages-count-change="onMessagesCountChange"
				 	v-on:message-status-change="onChangeStatus($event)"
					></messages-component>
			</div>
			<div class="col-md-4">
				<a href="#">Trash <span class="badge">@{{ messagesCount.trash }}</span></a>
				<messages-component class="messagesList"
					v-bind:messages="messages"
					status="trash"
				 	v-on:messages-count-change="onMessagesCountChange"
				 	v-on:message-status-change="onChangeStatus($event)"
				 	></messages-component>
			</div>
		</div>
		<hr/>

	</div>

<script>
$(function()
{
});

function getMessages(event)
{
	manager.getMessages(event);
}

function Error(message)
{
	alert(message);
}
</script>

@endsection
