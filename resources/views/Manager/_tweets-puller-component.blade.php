@verbatim

<script type="x-template" id="tweets-puller-component-template">
<div class="panel panel-default" >
	<div class="panel-heading">
		Twitter control
		<span class="badge">{{ lastTweetsFoundCount }}</span>
	</div>
	<div class="panel-body">
		<div v-if="error" class="alert alert-warning " role="alert">
			<button type="button" class="close" v-on:click="error = ''" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
			<strong>Warning!</strong> {{ error }}
		</div>
		<!--
		<button v-on:click="searchTweets">pull</button>
		-->
		<button v-on:click="resetSearch">reset</button>
		<input v-model.lazy="query" type="text" /> <span class="help-block"> {{query}} </span>
	</div>
</div>
</script>

<script type="text/javascript">

Vue.component('tweets-puller-component',
{
	props: [],
	template: '#tweets-puller-component-template',
	data: function ()
	{
		return {
			query: '',
			searchTimer: '',
			searchFreq: 10000,
			lastTweetsFoundCount: 0,
			error: ''
		}; 
	},
	created: function()
	{
		//console.log('created');
	},
	mounted: function()
	{
		//console.log('monted');
	    this.searchTweets();
	    //this.timer = setInterval( this.searchTweets, 3 * 1000 );

	},
	beforeDestroy: function()
	{
		this.cancelAutoUpdate();
	},
	methods: {

	    cancelAutoUpdate: function()
	    {
		    clearInterval( this.searchTimer );
		},

		searchTweets: function(event)
		{
			var that = this ;

			if( typeof optionalThat !== 'undefined' )
			{
				if( optionalThat instanceof MouseEvent )
				{
				}
				else
				{
					that = optionalThat ;
				}
			}

			if( this.query.trim() == '' )
			{
				//this.error = 'Empty query' ;
				that.searchTimer = setTimeout( that.searchTweets, that.searchFreq, that);
				return ;
			}

			var button = null ;
			if( typeof optionalThat !== 'undefined' && (optionalThat instanceof MouseEvent) )
			{
				button = event.target ;
				button.disabled = true ;					
			}

			// encodeURIComponent() for dash '#'
			$.getJSON('/Twitter/search?q='+encodeURIComponent(encodeURI(this.query)))
			.done(function(json)
			{
				//console.log( json );
				that.lastTweetsFoundCount = json.tweets_count ;
				console.log( 'tweets found: ' + json.tweets_count );
			})
			.fail(function(jqxhr, textStatus, error)
			{
				try
				{
					var json = $.parseJSON( jqxhr.responseText );
					that.error = json.error.message ;
				}
				catch(ex)
				{
					var matches = jqxhr.responseText.match( /<span class="exception_message">(.*)<\/span>/ );
					that.error = matches[1] ;
				}
			})
			.always(function()
			{
				that.searchTimer = setTimeout( that.searchTweets, that.searchFreq, that);
				if( button != null )
					button.disabled = false ;
			});

	    },

	    resetSearch: function()
	    {
			var that = this ;

			if( typeof optionalThat !== 'undefined' )
			{
				if( optionalThat instanceof MouseEvent )
				{
				}
				else
				{
					that = optionalThat ;
				}
			}
			var button = null ;
			if( typeof optionalThat !== 'undefined' && (optionalThat instanceof MouseEvent) )
			{
				button = event.target ;
				button.disabled = true ;					
			}

			$.getJSON('/Twitter/resetSearch')
			.done(function(json)
			{
				console.log( 'resetSearch done.');
			})
			.fail(function(jqxhr, textStatus, error)
			{
				try
				{
					var json = $.parseJSON( jqxhr.responseText );
					that.error = json.error.message ;
				}
				catch(ex)
				{
					var matches = jqxhr.responseText.match( /<span class="exception_message">(.*)<\/span>/ );
					that.error = matches[1] ;
				}
			})
			.always(function()
			{
				if( button != null )
					button.disabled = false ;
			});

	    }

	}
});
</script>

@endverbatim
