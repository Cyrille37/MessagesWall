@verbatim

<script type="x-template" id="display-settings-component-template">

<div class="panel panel-default" >
	<div class="panel-heading">
		Display settings
	</div>
	<div class="panel-body">
		<div v-if="error" class="alert alert-warning " role="alert">
			<button type="button" class="close" v-on:click="error = ''" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
			<strong>Warning!</strong> {{ error }}
		</div>
		<button v-on:click="saveSettings">save</button> <br/>
		freq ms: <input v-model="freq" type="number" size="6" />
	</div>
</div>

</script>

<script type="text/javascript">

Vue.component('display-settings-component',
{
	props: [],
	template: '#display-settings-component-template',
	data: function ()
	{
		return {
			freq: 8000,
			error: ''
		}; 
	},
	mounted: function()
	{
	},
	beforeDestroy: function()
	{
	},
	methods:
	{
		saveSettings: function(event)
		{
			var button = null ;
			if( typeof event !== 'undefined' )
			{
				button = event.target ;
				button.disabled = true ;
			}

			var that = this ;
			// encodeURIComponent() for dash '#'
			$.getJSON('/MsgWall//options/displayFreq/'+this.freq+'')
			.done(function(json)
			{
			})
			.fail(function(jqxhr, textStatus, error)
			{
				try
				{
					var json = $.parseJSON( jqxhr.responseText );
					that.error = json.error.message ;
				}
				catch(ex)
				{
					var matches = jqxhr.responseText.match( /<span class="exception_message">(.*)<\/span>/ );
					that.error = matches[1] ;
				}
			})
			.always(function()
			{
				if( button != null )
					button.disabled = false ;
			});

	    }
	}
});
</script>

@endverbatim
