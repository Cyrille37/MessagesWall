@verbatim

<script type="x-template" id="messages-component-template">

<ul class="list-group">
	<msg-component v-for="(msg, index) in messagesByStatus(status)"
		v-bind:msg="msg"
		v-bind:index="index"
		v-on:message-status-change="onChangeStatus($event,$event)"
	>
	</msg-component>
</ul>

</script>

<script type="x-template" id="messages-component-msg-template">

<transition name="slide-fade">
<li class="list-group-item panel panel-default">
	<h4 class="from">{{ msg.from }} <span class="badge">{{ msg.sent_at }}</span></h4>
	<p>{{ msg.text }}</p>
	
	<div>
		<template v-if="msg.raw.entities && msg.raw.entities.media">
			<span v-for="(m, i) in msg.raw.entities.media">
				<div v-if="m.type=='photo'">
					<br/><img :src="m.media_url+':thumb'" />
				</div>
				<span v-else > unknow media type <strong>{{m.type}}</strong> </span>
			</span>
		</template>
	</div>
	<div :id="'tweet-id-' + msg.id"></div>

	<template v-if="msg.status != 'onair'">
		<button type="button" v-on:click="changeStatus('onair')" class="btn btn-default" aria-label="on-air" :title="'put '+msg.id+ ' to onair'" >
			<span class="glyphicon glyphicon-send" aria-hidden="true"></span>
		</button>
	</template>
	<template v-if="msg.status != 'new'">
		<button type="button" v-on:click="changeStatus('new')" class="btn btn-default" aria-label="new" :title="'put '+msg.id+ ' to new'" >
			<span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
		</button>
	</template>
	<template v-if="msg.status != 'trash'">
		<button type="button" v-on:click="changeStatus('trash')" class="btn btn-default" aria-label="trash" :title="'put '+msg.id+ ' to trash'" >
			<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
		</button>
	</template>
</li>
</transition>

</script>

<script type="text/javascript">

Vue.component('messages-component',
{
	props: ['messages', 'status'],
	template: '#messages-component-template',
	data: function ()
	{
		return {
			messagesCount: 0
		}; 
	},
	methods:
	{
		/**
		 * Return only messages with status
		 */
		messagesByStatus: function( status )
		{
			//console.log('messagesByStatus: '+status);
			var a = this.messages.filter(function (item)
			{
				//console.log('message id: '+item.id);
				return item.status == status ;
			});
			if( this.messagesCount != a.length )
			{
				this.messagesCount = a.length ;
				this.$emit('messages-count-change', status, this.messagesCount );				
			}
			return a ;
		},

		/**
		 * Propagate message-status-change event to parent
		 */
		onChangeStatus: function( event )
		{
			this.$emit('message-status-change', event);
		}

	},

	components: {

		'msg-component': {
			props: ['msg', 'index'],
			template: '#messages-component-msg-template',
			methods:
			{
				/**
				 * Propagate message-status-change event to parent
				 */
				changeStatus: function( status )
				{
					this.$emit('message-status-change', { id: this.msg.id, status: status} );
				}

			},
			mounted: function(event)
			{
				switch( this.msg.type )
				{
				case MSG_TYPE_TWEET:
					// First time we see a message of type 'tweet', parse it's 'raw' property has json.
					if( ! this.msg.rawHasJson )
					{
						this.msg.raw = $.parseJSON(this.msg.raw) ;
						this.msg.rawHasJson = true ;	
					}
					break;

				case MSG_TYPE_SMS:
					break;
				}

			}

		}	

	}

})

</script>

@endverbatim
