<?php

namespace Twitter ;

class SearchMetaData {

	public $completed_in ;
	public $max_id ;
    public $max_id_str ;
    public $next_results ;
    public $query ;
    public $refresh_url ;
    public $count;
    public $since_id;
    public $since_id_str;

    public static function createFromArray( Array $object )
    {
    	$smd = new SearchMetaData();
    	$vars = get_object_vars($smd);
    	foreach( $vars as $k => $v )
    	{
    		if( isset($object[$k]) )
    		{
    			$smd->{$k} = $object[$k] ;
    		}
    	}
    	return $smd ;
    }

    public function asMoreResults()
    {
    	return $this->next_results!=null ? true : false ;
    }
}
