<?php

namespace MessagesWall ;

use Twitter\Status as Tweet ;
use MessagesWall\Models\Option ;
use MessagesWall\Models\Message ;
use App\Exceptions\SmsException;
use Carbon\Carbon ;

class MsgWallService
{
	//const DEFAULT_JSON_OPTIONS = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE ;
	const DEFAULT_JSON_OPTIONS = JSON_UNESCAPED_UNICODE ;
	const CACHE_LAST_TWEET_ID = 'msgwall_last_tweet_id';

	/**
	 * @param mixed $tweets
	 */
	public function addTweet( $tweets )
	{
		\DB::transaction(function () use ($tweets)
		{
			if( ! is_array($tweets) )
			{
				$tweets = [ $tweets ];
			}

			$lastTweetIdOriginal = $this->getLastTweetId();
			$lastTweetId = $lastTweetIdOriginal;
			
			foreach ($tweets as $tweet)
			{
				$msg = new Message();
				$msg->type = Message::TYPE_TWEET ;
				$msg->from = $tweet->getUser()->getName();
				$msg->to = null ;

				// Twitter date time string is an alien format :-)
				//error_log( $tweet->getCreatedAt() );
				// Fri Apr 21 07:41:53 +0000 2017
				$date = \DateTime::createFromFormat('D M j H:i:s e Y', $tweet->getCreatedAt());
				//error_log($date->format('Y-m-d H:i'));

				$msg->sent_at = $date;
				$msg->text = $tweet->getText();
				$msg->raw = json_encode( $tweet->getRaw() , self::DEFAULT_JSON_OPTIONS);

				$msg->save();

				if( $lastTweetId < $tweet->getId() )
					$lastTweetId = $tweet->getId() ;
			}

			if( $lastTweetIdOriginal < $lastTweetId )
			{
				Option::setLastTweetId($lastTweetId);
			}

		});

	}

	public function getLastTweetId()
	{
		return Option::getLastTweetId();
	}

	public function resetLastTweetId()
	{
		return Option::setLastTweetId( 0 );
	}

	public function statusChange( $msgId, $newStatus )
	{
		$msg = Message::findOrFail( $msgId );

		if( $msg->status != $newStatus )
		{
			$msg->status = $newStatus ;
			$msg->save();
		}
		return $msg ;
	}

	public function addSms( $sms )
	{
		if( ! isset($sms['body']) )
		{
			throw new SmsException('Sms has no "body" !!');
		}
		$configKey = Option::getSms2WebConfigKey();

		if( !empty($configKey) &&  preg_match('/^'.$configKey.'/', $sms['body']) )
		{
			// Don't store Config SMS, return silently
			return ;
		}

		/*
	    [sign] => 6707CA25A586C1FA8E8704E7355DC0649F8B39DF
	    [to] => +33695576204
	    [body] => Test noël forêt @#$/^&*()
	    [to_at] => 1493045222197
	    [from] => +33632330218
	    [from_at] => 1493045187000
	    [action] => push
	    [channel] => EE661A2D
		 */

		//error_log('saving sms');
		/**
		 * @var \MessagesWall\Models\Message $msg
		 */
		$msg = new Message();
		$msg->type = Message::TYPE_SMS ;
		$msg->from = $sms['from'];
		$msg->to = $sms['to'];

		// Timestamp, but what format ?
		$msg->sent_at = Carbon::createFromTimestampUTC( floor($sms['to_at']/1000) );
		error_log('sms sent_at: '.$msg->sent_at);

		$msg->text = $sms['body'];
		$msg->raw = '';

		$msg->save();

	}

}
