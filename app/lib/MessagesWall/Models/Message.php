<?php

namespace MessagesWall\Models ;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon ;

/**
 * @property integer $id
 * @property string $status
 * @property integer $status_changed_at
 * @property string $type
 * @property string $to
 * @property string $from
 * @property \Carbon\Carbon $sent_at
 * @property string $text ;
 * @property string $raw ;
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Message extends Model
{
	const STATUS_NEW = 'new' ;
	const STATUS_ONAIR = 'onair' ;
	const STATUS_TRASH = 'trash' ;
	const STATUS = [
		self::STATUS_NEW, self::STATUS_ONAIR, self::STATUS_TRASH
	];

	const TYPE_TWEET = 'tweet';
	const TYPE_SMS = 'sms';
	const TYPES = [
		self::TYPE_TWEET, self::TYPE_SMS
	];

	/**
	 * The attributes that are mass assignable.
	 * @var array
	 */
	protected $fillable = [
		'status', 'type', 'to', 'from', 'sent_at', 'text', 'raw'
	];
	
	/**
	 * Attributes that should be mutated to dates.
	 * @var array
	 */
	protected $dates = [
		/* dont't : 'status_changed_at', */
			
		'sent_at', 'created_at', 'updated_at'
	];

	/**
	 * Status "Mutator"
	 * https://laravel.com/docs/5.4/eloquent-mutators
	 * 
	 * @param string $value
	 */
	public function setStatusAttribute($value)
	{
		if( $this->status == $value )
			return ;
		$this->attributes['status'] = $value ;
		$this->attributes['status_changed_at'] = new Carbon() ;
	}

	public function getStatusChangedAtAttribute($value)
	{
		return (new Carbon($value))->timestamp ;
	}
	
	/**
	 * Onair "Local Scope"
	 * https://laravel.com/docs/5.4/eloquent#local-scopes
	 * 
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeStatusOnair($query)
	{
		return $query->where('status', '=', self::STATUS_ONAIR);
	}

}
