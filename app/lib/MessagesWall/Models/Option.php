<?php

namespace MessagesWall\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

/**
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Option extends Model
{
	const KEY_LAST_TWEET_ID = 'LastTweetId' ;
	const KEY_SMS2WEB_CONFIGKEY = 'Sms2WebConfigKey' ;

	/**
	 * The attributes that are mass assignable.
	 * @var array
	 */
	protected $fillable = [
		'key', 'value'
	];

	/**
	 * Attributes that should be mutated to dates.
	 * @var array
	 */
	protected $dates = [
		'created_at', 'updated_at'
	];

	/**
	 * Option must exists to be updated, no creation.
	 * 
	 * @param string $key
	 * @param string|integer $value
	 * @return boolean
	 */
	public static function setOption( $key, $value )
	{
		$option = Option::where( 'key', '=', $optionKey )->first();
		if( empty($option) )
			return false ;

		$option->value = $value ;
		$option->save();
		return true ;
	}

	public static function getLastTweetId()
	{
		try
		{
			$option = Option::where('key', '=', self::KEY_LAST_TWEET_ID)->firstOrFail();
			return $option->value ;
		}
		catch( ModelNotFoundException $ex )
		{}
		return null ;
	}

	public static function setLastTweetId( $tweet_id )
	{
		$option = Option::updateOrCreate(
				['key'=>self::KEY_LAST_TWEET_ID],
				['value'=>$tweet_id]
				);
	}
	
	public static function setSms2WebConfigKey( $config_key )
	{
		$option = Option::updateOrCreate(
				['key'=>self::KEY_SMS2WEB_CONFIGKEY],
				['value'=>$config_key]
				);
	}
	
	public static function getSms2WebConfigKey()
	{
		try
		{
			$option = Option::where('key', '=', self::KEY_SMS2WEB_CONFIGKEY)->firstOrFail();
			return $option->value ;
		}
		catch( ModelNotFoundException $ex )
		{}
		return null ;
	}
}
