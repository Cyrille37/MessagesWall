<?php

namespace App\Helpers ;

class DebugHelper {

	public static function dump(...$args)
	{
		foreach( $args as $x )
		{
			(new \Illuminate\Support\Debug\Dumper)->dump($x);
		}
	}
}
