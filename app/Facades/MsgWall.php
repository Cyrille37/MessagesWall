<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade ;
use App\Providers\AppServiceProvider;

/**
 * @see \MessagesWall\MsgWallService
 */
class MsgWall extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return AppServiceProvider::MSGWALL_SRV ;
	}
}
