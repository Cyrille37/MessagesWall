<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use MessagesWall\MsgWallService;

class AppServiceProvider extends ServiceProvider
{
	const MSGWALL_SRV = 'msgwallsrv' ;
	
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	// https://laravel.com/docs/5.4/migrations#indexes
    	// http://stackoverflow.com/questions/23786359/laravel-migration-unique-key-is-too-long-even-if-specified
    	Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    	//
    	// Dev tools
    	//
    	
    	// env: testing (phpunit.xml), local (.env), production...
    	$env = $this->app->environment();
    	if ($env != 'production') {
    		if ($env != 'testing') {
    			if ($this->app->runningInConsole()) {
    				$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
    			}
    			$this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
    		}
    	}

    	$this->app->singleton(self::MSGWALL_SRV, function ($app) {
    		return new MsgWallService();
    	});

    }
}
