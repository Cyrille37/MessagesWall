<?php

namespace App\Http\Controllers\MsgWall;

use App\Http\Controllers\Controller;
use MessagesWall\Models\Message ;
use Carbon\Carbon ;

class DisplayController extends Controller {

	/**
	 * How many messages delivered by request. 
	 * @var integer
	 */
	const GET_MSGS_COUNT = 10 ;

	public function index()
	{
		return view('Display.index');
	}

	/**
	 * Return fresh status changed (status_changed_at) messages.
	 * 
	 * @param integer optional timestamp
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function messagesStatusChanged( $since = 0 )
	{
		error_log(__METHOD__);

		$msgs= null ;
		if( empty($since) )
		{
			/*
			 * At start ($since is empty) get several last messages, forget older ones.
			 */
			$offset = Message::where('status','=',Message::STATUS_ONAIR)->count();
			$offset = $offset > self::GET_MSGS_COUNT ? $offset - self::GET_MSGS_COUNT : 0 ;

			$msgs= Message::where('status','=',Message::STATUS_ONAIR)
				->orderBy('status_changed_at', 'asc')
				->offset($offset)->limit(self::GET_MSGS_COUNT)->get();
		}
		else
		{
			/* 1. use '>=' not only '>' because
			 * several messages could get an identical status_changed_at.
			 * Let's the display manages duplicate messages.
			 * 
			 * 2. DON't select on 'status' because some messages could be deleted,
			 * and the display has to know to delete them. 
			 */
			$msgs= Message::where( 'status_changed_at', '>=', Carbon::createFromTimestamp($since) )
				->orderBy('status_changed_at', 'asc')
				->limit(self::GET_MSGS_COUNT)->get();
		}

		return response()->json( $msgs->toArray() );
	}

}
