<?php

namespace App\Http\Controllers\MsgWall;

use App\Http\Controllers\Controller;
use MessagesWall\Models\Message ;
use MessagesWall\Models\Option;

class ManagerController extends Controller
{
	public function index()
	{
		return view('Manager.index');
	}

	public function statusChange( $msgId, $newStatus )
	{
		\MsgWall::statusChange( $msgId, $newStatus );

		return response()->json( ['success'=>true ] );
	}

	/**
	 * Retreive all messages, or all messages after $fromId Incremental Primary Key.
	 * No specific order, let's client managing order.
	 *  
	 * @param integer $fromId
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function messages( $fromId = null )
	{
		$msgs = null ;
		if( empty($fromId))
		{
			$msgs = Message::all();
		}
		else
		{
			$msgs = Message::where('id', '>', $fromId )->get();
		}
		return response()->json( $msgs );
	}

	/**
	 * Set an Option model
	 * @param string $optionKey
	 * @param integer|string $optionValue
	 */
	public function setOption( $optionKey, $optionValue )
	{
		$ok = Option::setOption( $optionKey, $optionValue);
		return response()->json( ['success'=>$ok] );
	}
	
	public function getOption( $optionKey )
	{
		$option = Option::where('key','=',$optionKey)->first();
		if( empty($option) )
			return response()->json( ['success'=>false] );
		return response()->json(
			[
			'success'=>true,
			'key'=>$optionKey,
			'value'=>$option->value
			] );
	}
}
