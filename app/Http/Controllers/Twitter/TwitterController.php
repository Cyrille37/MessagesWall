<?php

namespace App\Http\Controllers\Twitter ;

use App\Http\Controllers\Controller;
use Twitter ;
use Twitter\TwitterAPI;
use Illuminate\Http\Request;
use App\Exceptions\TwitterException ;
use App\Exceptions\TwitterExceptionAlreadySearching;
use App\Facades\MsgWall;

/**
 * Twitter credentials are stored in a encrypted cookie.
 * 
 * @author cyrille
 */
class TwitterController extends Controller {
	
	const CREDS_COOKIE = 'twitter-creds' ;
	const CACHE_SEARCH_LOCK = 'twitter-search-lock' ;

	public function index()
	{
		$twUser = null ;

		try
		{
			$tw = $this->getTwitterApi();
			$twUser = $tw->verifyCredentials();
		}
		catch( TwitterException $ex )
		{
		}

		return view('Twitter.index', ['twUser'=>$twUser]);
	}

	public function resetSearch()
	{
		\MsgWall::resetLastTweetId();
		return response()->json( ['success'=>true] );
	}

	public function creds( Request $request )
	{
		return view('Twitter.creds');
	}

	public function setCreds( Request $request )
	{
		$creds = [
			'ConsumerKey' => $request->input('ConsumerKey'),
			'ConsumerSecret' => $request->input('ConsumerSecret'),
			'UserId' => $request->input('UserId'),
			'AccessToken' => $request->input('AccessToken'),
			'AccessTokenSecret' => $request->input('AccessTokenSecret'),
		];
		foreach( $creds as $k=>$v )
		{
			if( empty($v) )
				throw new TwitterException('Twitter Credential "'.$k.'" Must Be Set !!');
		}

		$tw = new TwitterAPI( $creds['ConsumerKey'] , $creds['ConsumerSecret']);
		$tw->setAccessToken($creds['UserId'], $creds['AccessToken'], $creds['AccessTokenSecret']);

		$twUser = $tw->verifyCredentials();

		if( $twUser==null || ($twUser->getId() != $creds['UserId']) )
			throw new TwitterException('Twitter OAuth Failed !!');

		// Store creds into a cookie
		return redirect('/Twitter/')
			->cookie(self::CREDS_COOKIE, $creds );
	}

	/**
	 * 
	 * @return \Twitter\TwitterAPI
	 */
	protected function getTwitterApi()
	{
		$creds = \Cookie::get(self::CREDS_COOKIE);
		if( empty($creds) || ! is_array($creds) )
		{
			throw new TwitterException('Twitter Credentials Must Be Set !!');
		}
		
		$tw = new TwitterAPI( $creds['ConsumerKey'] , $creds['ConsumerSecret']);
		$tw->setAccessToken($creds['UserId'], $creds['AccessToken'], $creds['AccessTokenSecret']);

		return $tw ;
	}

	public function search( Request $request )
	{
		// Becarefull to encode the dash '#' as '%23', because dash has a meaning in url, it's an anchor ;-)
		// like http://127.0.0.1:8000/Twitter/search?q=%23PTCE

		$query = $request->input('q');

		if( empty($query) )
			throw new TwitterException('Query "q" Must Be Set !!');

		$tw = $this->getTwitterApi();

		if( ! empty( \Cache::get(self::CACHE_SEARCH_LOCK, null) ) )
			throw new TwitterExceptionAlreadySearching('Already searching');

		$results = [
			'tweets_count' => 0 ,
			'twitter_time' => 0
		];

		try
		{
			\Cache::forever( self::CACHE_SEARCH_LOCK, 1);

			// GET search/tweets : https://dev.twitter.com/rest/reference/get/search/tweets
			$lastTweetId = MsgWall::getLastTweetId();
			
			$twitter_time = microtime(true);

			$tweets = $tw->searchTweets(
				$query,
				[
					'lang'=>'fr',
					'since_id'=>$lastTweetId,
					'include_entities' => true
				],
				50 );

			$results['twitter_time'] = microtime(true) - $twitter_time ;
			$results['tweets_count'] = count($tweets);

			MsgWall::addTweet( $tweets);

		}
		catch( \Exception $ex )
		{
			throw $ex ;
		}
		finally
		{
			\Cache::forget(self::CACHE_SEARCH_LOCK);
		}

		if( $request->wantsJson() )
		{
			return response()->json( $results );
		}
		return view('Twitter.search', ['results'=>$results]);
	}

}
