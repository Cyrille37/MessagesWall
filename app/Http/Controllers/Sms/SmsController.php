<?php

namespace App\Http\Controllers\Sms ;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Nexmo\Client as NexmoClient ;

class SmsController extends Controller
{

	public function index()
	{
		return view('Sms.index');
	}

	public function nexmo( Request $request, NexmoClient $nexmo)
	{
		//error_log( print_r($nexmo,true) );
		
		$message = \Nexmo\Message\InboundMessage::createFromGlobals();
		if($message->isValid())
		{
			if( isset($message['udh']) && !empty($message['udh']) )
			{
				// Not a SMS, perhaps a MMS
			}
			else
			{
				error_log('Got a message');
			}
		}


/*
		//error_log( print_r($message ,true) );
		
		if( isset($message['udh']) && !empty($message['udh']) )
		{
			foreach( $request->all() as $k => $v )
			{
				switch($k)
				{
					case 'text':
					case 'keyword':
						error_log($k.'='. bin2hex($v) );
						break;
					default:
						error_log($k.'='. $v );
				}
			}
		}
		else 
		{
			error_log('Encodings: '.print_r( $request->all(),true));

		}

		if($message->isValid())
		{
			error_log('Got a message');

			error_log("\t type: ".$message->getType() );
			error_log("\t to: ".$message->getTo() );
			error_log("\t from: ".$message->getFrom() );
			error_log("\t body: ".$message->getBody() );
			error_log("\t messageId: ".$message->getMessageId());
			error_log("\t network: ".$message->getNetwork() );
			error_log("\t accountId: ".$message->getAccountId());

			error_log("\t msisdn: ". (isset($message['message-timestamp'])?$message['message-timestamp']:'NULL') );
			error_log("\t msisdn: ". (isset($message['msisdn'])?$message['msisdn']:'NULL') );
			error_log("\t keyword: ". (isset($message['keyword'])?$message['keyword']:'NULL') );
			error_log("\t concat: ". (isset($message['concat'])?$message['concat']:'NULL') );
			error_log("\t data: ". (isset($message['data'])?$message['data']:'NULL') );
			error_log("\t udh: ". (isset($message['udh'])?$message['udh']:'NULL') );
			
		} else {
			error_log('Not a message');
		}
*/
		return 'Ok';
	}

	public function sms2web_post(Request $req )
	{
		/*
	    [sign] => 8E118BE3CA577ADD09797D7FE912D5ED3C7CE83B
	    [to] => 
	    [from] => +33695576204
	    [action] => pull
	    [channel] => EE661A2D

	    [sign] => 6707CA25A586C1FA8E8704E7355DC0649F8B39DF
	    [to] => +33695576204
	    [body] => Test noël forêt @#$/^&*()
	    [to_at] => 1493045222197
	    [from] => +33632330218
	    [from_at] => 1493045187000
	    [action] => push
	    [channel] => EE661A2D

	    [sign] => C21BB1381579134BC80041A1286EF11406ACA7DD
	    [to] => +33695576204
	    [body] => @789
	url=http://192.168.43.219:8091/api/Sms/Sms2Web
	    [to_at] => 1493198683226
	    [from] => +33632330218
	    [from_at] => 1493198680000
	    [action] => push
	    [channel] => EE661A2D
		 */

		$action = $req->get('action');
		
		error_log(__METHOD__.' action:'.$action);
		
		switch($action)
		{
			case 'pull':
				break;

			case 'push':
				error_log( print_r(\Request::all(),true) );
				$sms = \Request::all() ;
				\MsgWall::addSms( $sms );

				break;
			default:
				return response()->json( ['succes'=>false] );
		}

		return response()->json( ['succes'=>true] );
	}

}
