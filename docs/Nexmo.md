# Nexmo

- Tutorial: https://laravel-news.com/sending-receiving-sms-laravel-nexmo
- Laravel 5.4 notifications: https://laravel.com/docs/5.4/notifications#sms-notifications

## Tech

### MMS

Nexmo dit ne pas gérer les MMS
https://help.nexmo.com/hc/en-us/articles/218436907-Why-doesn-t-Nexmo-offer-MMS-

Mais les MMS sont bien renvoyés mais indécodable:
- le "type" = text
- le "udh" a une valeur (pas pour les SMS)
- le "from" = 555

### Dump Nexmo's Webhook verification

user@user-W740SU:~/Code/www/MessagesWall/www$ ./artisan serve --host=0.0.0.0 --port=8091
Laravel development server started: <http://0.0.0.0:8091>

#### via GET

[Sat Apr 22 12:17:39 2017] Illuminate\Http\Request::__set_state(array(
   'json' => NULL,
   'convertedFiles' => NULL,
   'userResolver' => 
  Closure::__set_state(array(
  )),
   'routeResolver' => 
  Closure::__set_state(array(
  )),
   'attributes' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'request' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'query' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'server' => 
  Symfony\Component\HttpFoundation\ServerBag::__set_state(array(
     'parameters' => 
    array (
      'DOCUMENT_ROOT' => '/home/user/Code/www/MessagesWall/www/public',
      'REMOTE_ADDR' => '174.36.197.202',
      'REMOTE_PORT' => '56308',
      'SERVER_SOFTWARE' => 'PHP 7.1.4-1+deb.sury.org~xenial+1 Development Server',
      'SERVER_PROTOCOL' => 'HTTP/1.1',
      'SERVER_NAME' => '0.0.0.0',
      'SERVER_PORT' => '8091',
      'REQUEST_URI' => '/api/Sms/Nexmo?',
      'REQUEST_METHOD' => 'GET',
      'SCRIPT_NAME' => '/index.php',
      'SCRIPT_FILENAME' => '/home/user/Code/www/MessagesWall/www/public/index.php',
      'PATH_INFO' => '/api/Sms/Nexmo',
      'PHP_SELF' => '/index.php/api/Sms/Nexmo',
      'HTTP_ACCEPT' => '*/*',
      'HTTP_USER_AGENT' => 'Nexmo/MessagingHUB/v1.0',
      'HTTP_HOST' => 'ahost.ddns.net:8091',
      'REQUEST_TIME_FLOAT' => 1492856259.357634,
      'REQUEST_TIME' => 1492856259,
    ),
  )),
   'files' => 
  Symfony\Component\HttpFoundation\FileBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'cookies' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'headers' => 
  Symfony\Component\HttpFoundation\HeaderBag::__set_state(array(
     'headers' => 
    array (
      'accept' => 
      array (
        0 => '*/*',
      ),
      'user-agent' => 
      array (
        0 => 'Nexmo/MessagingHUB/v1.0',
      ),
      'host' => 
      array (
        0 => 'ahost.ddns.net:8091',
      ),
    ),
     'cacheControl' => 
    array (
    ),
  )),
   'content' => NULL,
   'languages' => NULL,
   'charsets' => NULL,
   'encodings' => NULL,
   'acceptableContentTypes' => NULL,
   'pathInfo' => '/api/Sms/Nexmo',
   'requestUri' => '/api/Sms/Nexmo?',
   'baseUrl' => '',
   'basePath' => NULL,
   'method' => 'GET',
   'format' => NULL,
   'session' => NULL,
   'locale' => NULL,
   'defaultLocale' => 'en',
   'isForwardedValid' => true,
))

#### via POST

[Sat Apr 22 12:42:29 2017] Illuminate\Http\Request::__set_state(array(
   'json' => NULL,
   'convertedFiles' => NULL,
   'userResolver' => 
  Closure::__set_state(array(
  )),
   'routeResolver' => 
  Closure::__set_state(array(
  )),
   'attributes' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'request' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'query' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'server' => 
  Symfony\Component\HttpFoundation\ServerBag::__set_state(array(
     'parameters' => 
    array (
      'DOCUMENT_ROOT' => '/home/user/Code/www/MessagesWall/www/public',
      'REMOTE_ADDR' => '174.36.197.202',
      'REMOTE_PORT' => '65292',
      'SERVER_SOFTWARE' => 'PHP 7.1.4-1+deb.sury.org~xenial+1 Development Server',
      'SERVER_PROTOCOL' => 'HTTP/1.1',
      'SERVER_NAME' => '0.0.0.0',
      'SERVER_PORT' => '8091',
      'REQUEST_URI' => '/api/Sms/Nexmo',
      'REQUEST_METHOD' => 'POST',
      'SCRIPT_NAME' => '/index.php',
      'SCRIPT_FILENAME' => '/home/user/Code/www/MessagesWall/www/public/index.php',
      'PATH_INFO' => '/api/Sms/Nexmo',
      'PHP_SELF' => '/index.php/api/Sms/Nexmo',
      'HTTP_ACCEPT' => '*/*',
      'HTTP_USER_AGENT' => 'Nexmo/MessagingHUB/v1.0',
      'HTTP_HOST' => 'ahost.ddns.net:8091',
      'CONTENT_LENGTH' => '0',
      'HTTP_CONTENT_LENGTH' => '0',
      'CONTENT_TYPE' => 'application/x-www-form-urlencoded; charset=UTF-8',
      'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded; charset=UTF-8',
      'REQUEST_TIME_FLOAT' => 1492857749.512126,
      'REQUEST_TIME' => 1492857749,
    ),
  )),
   'files' => 
  Symfony\Component\HttpFoundation\FileBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'cookies' => 
  Symfony\Component\HttpFoundation\ParameterBag::__set_state(array(
     'parameters' => 
    array (
    ),
  )),
   'headers' => 
  Symfony\Component\HttpFoundation\HeaderBag::__set_state(array(
     'headers' => 
    array (
      'accept' => 
      array (
        0 => '*/*',
      ),
      'user-agent' => 
      array (
        0 => 'Nexmo/MessagingHUB/v1.0',
      ),
      'host' => 
      array (
        0 => 'ahost.ddns.net:8091',
      ),
      'content-length' => 
      array (
        0 => '0',
      ),
      'content-type' => 
      array (
        0 => 'application/x-www-form-urlencoded; charset=UTF-8',
      ),
    ),
     'cacheControl' => 
    array (
    ),
  )),
   'content' => NULL,
   'languages' => NULL,
   'charsets' => NULL,
   'encodings' => NULL,
   'acceptableContentTypes' => NULL,
   'pathInfo' => '/api/Sms/Nexmo',
   'requestUri' => '/api/Sms/Nexmo',
   'baseUrl' => '',
   'basePath' => NULL,
   'method' => 'POST',
   'format' => NULL,
   'session' => NULL,
   'locale' => NULL,
   'defaultLocale' => 'en',
   'isForwardedValid' => true,
))

#### via Nexmo\Client

[Sat Apr 22 12:50:23 2017] Nexmo\Client Object
(
    [credentials:protected] => Nexmo\Client\Credentials\Basic Object
        (
            [credentials:protected] => Array
                (
                    [api_key] => xxxxxxxx
                    [api_secret] => xxxxxxxxxxxxxxxxx
                )

        )

    [client:protected] => Http\Adapter\Guzzle6\Client Object
        (
            [client:Http\Adapter\Guzzle6\Client:private] => GuzzleHttp\Client Object
                (
                    [config:GuzzleHttp\Client:private] => Array
                        (
                            [handler] => GuzzleHttp\HandlerStack Object
                                (
                                    [handler:GuzzleHttp\HandlerStack:private] => Closure Object
                                        (
                                            [static] => Array
                                                (
                                                    [default] => Closure Object
                                                        (
                                                            [static] => Array
                                                                (
                                                                    [default] => GuzzleHttp\Handler\CurlMultiHandler Object
                                                                        (
                                                                            [factory:GuzzleHttp\Handler\CurlMultiHandler:private] => GuzzleHttp\Handler\CurlFactory Object
                                                                                (
                                                                                    [handles:GuzzleHttp\Handler\CurlFactory:private] => 
                                                                                    [maxHandles:GuzzleHttp\Handler\CurlFactory:private] => 50
                                                                                )

                                                                            [selectTimeout:GuzzleHttp\Handler\CurlMultiHandler:private] => 1
                                                                            [active:GuzzleHttp\Handler\CurlMultiHandler:private] => 
                                                                            [handles:GuzzleHttp\Handler\CurlMultiHandler:private] => Array
                                                                                (
                                                                                )

                                                                            [delays:GuzzleHttp\Handler\CurlMultiHandler:private] => Array
                                                                                (
                                                                                )

                                                                        )

                                                                    [sync] => GuzzleHttp\Handler\CurlHandler Object
                                                                        (
                                                                            [factory:GuzzleHttp\Handler\CurlHandler:private] => GuzzleHttp\Handler\CurlFactory Object
                                                                                (
                                                                                    [handles:GuzzleHttp\Handler\CurlFactory:private] => 
                                                                                    [maxHandles:GuzzleHttp\Handler\CurlFactory:private] => 3
                                                                                )

                                                                        )

                                                                )

                                                            [parameter] => Array
                                                                (
                                                                    [$request] => <required>
                                                                    [$options] => <required>
                                                                )

                                                        )

                                                    [streaming] => GuzzleHttp\Handler\StreamHandler Object
                                                        (
                                                            [lastHeaders:GuzzleHttp\Handler\StreamHandler:private] => Array
                                                                (
                                                                )

                                                        )

                                                )

                                            [parameter] => Array
                                                (
                                                    [$request] => <required>
                                                    [$options] => <required>
                                                )

                                        )

                                    [stack:GuzzleHttp\HandlerStack:private] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [0] => Closure Object
                                                        (
                                                            [parameter] => Array
                                                                (
                                                                    [$handler] => <required>
                                                                )

                                                        )

                                                    [1] => prepare_body
                                                )

                                        )

                                    [cached:GuzzleHttp\HandlerStack:private] => 
                                )

                            [allow_redirects] => Array
                                (
                                    [max] => 5
                                    [protocols] => Array
                                        (
                                            [0] => http
                                            [1] => https
                                        )

                                    [strict] => 
                                    [referer] => 
                                    [track_redirects] => 
                                )

                            [http_errors] => 1
                            [decode_content] => 1
                            [verify] => 1
                            [cookies] => 
                            [headers] => Array
                                (
                                    [User-Agent] => GuzzleHttp/6.2.1 curl/7.47.0 PHP/7.1.4-1+deb.sury.org~xenial+1
                                )

                        )

                )

        )

    [factory:protected] => Nexmo\Client\Factory\MapFactory Object
        (
            [map:protected] => Array
                (
                    [message] => Nexmo\Message\Client
                    [verify] => Nexmo\Verify\Client
                    [applications] => Nexmo\Application\Client
                    [numbers] => Nexmo\Numbers\Client
                    [calls] => Nexmo\Call\Collection
                )

            [cache:protected] => Array
                (
                )

            [client:protected] => Nexmo\Client Object
 *RECURSION*
        )

    [options:protected] => Array
        (
            [api_key] => xxxxxxxx
            [api_secret] => xxxxxxxxxxxxxxxxx
            [signature_secret] => 
        )

)




