# Messages wall

<p align="center">Done with <img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Twitter auth

Go to https://apps.twitter.com/

## Tips & tricks

### local server

You can change host & port of the local server :
```
./artisan serve --host=0.0.0.0 --port=8091
```

### Cleanup Laravel cache & code tools

```
./artisan clear-compiled && ./artisan cache:clear && ./artisan config:clear && ./artisan route:clear && ./artisan view:clear \
  && ./artisan optimize && ./artisan ide-helper:generate
```

## License

Messages wall is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
