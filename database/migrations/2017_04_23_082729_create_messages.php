<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status', 10)
            	->default( \MessagesWall\Models\Message::STATUS_NEW );
            $table->timestamp('status_changed_at');
            $table->string('type', 10);
            $table->string('to')
            	->nullable();
            $table->string('from');
            $table->dateTime('sent_at');
            $table->text('text',255);
            $table->longText('raw')
            	->nullable();            
            $table->timestamps();

            $table->index('status');
            $table->index('status_changed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
